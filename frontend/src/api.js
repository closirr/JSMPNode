import axios from 'axios';
const serverUrl = 'http://localhost:4000';

let index = 3;
let devices = {
    1: {
        id: 1,
        name: 'Device #1',
        address: '192.168.1.50',
        port: 90,
        state: 'on'
    },
    2: {
        id: 2,
        name: 'Device #2',
        address: '192.168.1.60',
        port: 80,
        state: 'off'
    }
};

export async function getDevices() {
    const response = await axios.get(serverUrl + '/devices');
    return response.data;
}

export async function getDeviceById(deviceId) {
    const response = await axios.get(`${serverUrl}/devices/${deviceId}`);
    return response.data;
}

export async function addDevice(device) {
    index += 1;
    // {
    //     id: index,
    //     state: 'off',
    //     ...device

    const response = await axios.post(`${serverUrl}/devices/`, device);

    if (response.status !== 201 ) {
        throw new Error('device not connected');
    }
}

export async function removeDevice(deviceId) {
    const response = await axios.delete(`${serverUrl}/devices/${deviceId}`);

}

export async function updateDevice(deviceId, data) {
    const response = await axios.put(`${serverUrl}/devices/${deviceId}`,data);

}

export async function switchOn(deviceId) {
    await axios.put(`${serverUrl}/devices/${deviceId}/on`);
}

export async function switchOff(deviceId) {
    await axios.put(`${serverUrl}/devices/${deviceId}/off`);
}

export async function getDeviceLog(deviceId) {
    const response = await axios.get(`${serverUrl}/devices/log/${deviceId}`);
    return response.data;
}