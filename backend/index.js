const express = require('express');
const mongoose = require('mongoose');
const deviceRouter = require('./devices');

const app = express();
const port = 4000;
app.use(express.json());
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,DELETE,POST');
    next();
});

app.use('/devices', deviceRouter);

app.get('/', (req, res) => {
    res.send('It stsill works!');
});

app.listen(port, () => {
    console.log('server has been started on port ' + port);
})

mongoose.connect('mongodb://localhost/smartHome');
