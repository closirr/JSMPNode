/* jshint esversion: 6, unused: true */

const router = require('express').Router();
const Device = require('./models/device');
const http = require('http');

let index = 3;
let devices = {
    1: {
        id: 1,
        name: 'Device #1',
        address: '192.168.1.50',
        port: 90,
        state: 'on'
    },
    2: {
        id: 2,
        name: 'Device #2',
        address: '192.168.1.60',
        port: 80,
        state: 'off'
    }
};

const deviceAdapter = (device) => ({
    id: device._id,
    name: device.name,
    address: device.address,
    state: device.state,
    port: device.port

});

function sendRequest(url) {
    return new Promise((resolve, reject) => {
        http.get(url, (res) => {
            if (res.statusCode !== 200) {
                reject(res.statusCode);
            }
            else {
                resolve();
            }
        });
    });
}

router.get('/', async (req, res) => {
    const devices = await Device.find().exec();
    res.json(devices.map(deviceAdapter));
})

router.get('/:id', async (req, res) => {
    const deviceId = req.params.id;
    const device = await Device.findById(deviceId).exec();

    if (device) {
        res.json(deviceAdapter(device));
    }
    else {
        res.sendStatus(404);

    }
})

router.post('/', (req, res) => {
    const device = req.body;
    if (!device) {
        res.sendStatus(400);
    }
    index += 1;

    Device.create({
        // id: index,
        state: 'off',
        log: [],
        ...device
    });

    res.sendStatus(201);
});

router.put('/:id', async (req, res) => {
    const deviceId = req.params.id;
    const data = req.body;

    await Device.findByIdAndUpdate(deviceId, data).exec();

    res.sendStatus(200);

});

router.delete('/:id', async (req, res) => {
    const deviceId = req.params.id;

    await Device.findByIdAndRemove(deviceId);

    res.sendStatus(200);
});

router.put('/:id/:cmnd', async (req, res) => {
    const deviceId = req.params.id;
    const device = await Device.findById(deviceId).exec();

    const commands = {
        'on': 'Power%20On',
        'off': 'Power%20off'
    }
    const command = commands[req.params.cmnd];

    const url = `http://${device.address}:${device.port}/cm?cmnd=${command}`;

    await sendRequest(url);

    device.state = req.params.cmnd;
    device.log.push({date: new Date(), action: req.params.cmnd})
    await device.save();

    res.sendStatus(200);
})

router.get('/log/:deviceId', async (req, res) => {
    const device = await Device.findById(req.params.deviceId).exec();
    res.json(device.log);
})

module.exports = router;

